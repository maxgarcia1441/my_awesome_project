# Digital Fabrication group 17

Members:

> Max Garcia Hinojosa

> Elemeri Lautamatti

> Tuomas Numminen


This is the documentation page for our project, you can also find the weekly progress we made here.

## Week 1

The goals of the first week where to reach out to all of the group members and to meet up and discuss possible ideas for the project. We never reached one group member, but we made it work with 3 members. Us 3 met up and soon enough we had the idea for the project, an rc car controlled by Bluetooth. Elmeri made a preliminary model of the car and Tuomas and Max constructed the introductory report.

## Week 2

We met up at Fablab after picking up components so we could measure them and construct a model. The measurement process took about an hour. The goal for this week is to make a model of the chassis so we can start printing it next week. Elmeri designed the chassis in Solidworks and this process too about one hour. We refined our idea and chose to also add a distance sensor to the car to stop it from crashing.

## Week 3

The goal for this week was to model the car completely so it could be printed. Elmeri designed mounts for motors and servos. It took about two and a half hours to finish the model of the car. It was relatively simple since I have experience 3D modeling. We also met at Fablab to find screws that we would use to attach components to the chassis. Originally, I thought we would use a screw with a wide, flat head, but those were not available. We chose to use washers and a normal screw. The screw was supposed to attach the wishbone to the chassis and steering rack. Tuomas started modeling the tire. Max started printing the chassis once it was finished. The print time was about 5 hours. The print failed due to wrong settings in the printed and thus needed to be printed again. 

![](images/Whole_Car.jpg)

## Week 4

This week’s goal was to print the chassis and mount components to the chassis, like the motor and servo. Max printed the chassis again and this time it worked out. The print time was 5h again and after that the whole group met up at Fablab to start to assemble it. Assembly without the tires took about one and a half hour. Tuomas had finished the model of the tire and the process took about 3 hours using Blender. 

## Week 5

The goal of the week is to print wheels and motor shafts for the wheels and to start coding. There were some issues with Tuomas designing the wheel in Blender and Elmeri designing the motor shaft in Solidworks. The scaling did not work and due to that the shaft did not fit the wheels. Tuomas modeled the shaft again and now it fit his wheels perfectly. This took about one hour. Max started the coding process as he had experience coding beforehand. The code took about 3 hours to write and was ready sooner than expected. 

## Week 6

The goal was to finish the project. Now that the wheels and motor shaft is printed, we installed them and uploaded the code to the Arduino. The final touches to the project took about 2 and a half hours and all of us were involved in it. The final product worked as intended but the wheels just needed more grip. The distance sensor worked well and stopped the car from crashing. Only thing we were missing was the shell to make the car more aesthetically pleasing and to protect the Arduino and other components. 

![](images/screen_2.jpg)

> *No shell this time :(*

## Final Result

### Demo 1

<iframe width="560" height="315" src="https://www.youtube.com/embed/SgSludpvvQI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

If the video doesn't display, follow this [link](https://drive.google.com/file/d/1nXMpozQFd29kLYWfXBi6_tHOgk91DFkK/preview)

### Demo 2

<iframe width="560" height="315" src="https://www.youtube.com/embed/mI8e1skAnls" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

If the video doesn't display, follow this [link](https://drive.google.com/file/d/1yTAPsPr1I2z025JU1gk6igGpcoJfc8H6/preview)
