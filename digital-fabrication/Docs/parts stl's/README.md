# Images

> ***All of the following parts were 3D printed at Fab Lab.***

Battery Holder

![](../images/Battery_Holder.jpg)

Bottom

![](../images/Bottom.jpg)

Car with screws

![](../images/Car_with_screws.jpg)

Chassis

![](../images/Chassis.jpg)

Distance sensor and holder

![](../images/Distance_sensor_and_holder.jpg)

Exploded View

![](../images/Exploded_view.jpg)

Steering setup *(The gear was laser-cut)*

![](../images/Steering_setup.jpg)

Wishbone

![](../images/Wishbone.jpg)
