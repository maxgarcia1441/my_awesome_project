// Maximiliano Garcia Hinojosa 28.04.2022

#include <Servo.h>

//directions: f, forward. b, backward.

String incoming;   // for incoming serial data
char popo; // Char from serial
Servo servo; // Creating servo object
const int motor_1 = 3;
const int motor_2 = 5;
int power = 255;
int angle;

// How much the sservo will turn every time

int steps = 30;

// Distance sensor pins

const int echoPin = 2;
const int trigPin = 3;

//Distance sensor variables

int distance; //in cm
long duration;

int counter;
boolean state = false;

void ang(int a){
  angle = map(a,0,180,30,178);
}

void to_angle(){
  servo.write(angle);
}

void forward(){
  digitalWrite(motor_2, LOW);
  analogWrite(motor_1, power);
}

void back(){
  digitalWrite(motor_1, LOW);
  analogWrite(motor_2, power);
  }

void halt(){
  digitalWrite(motor_1, LOW);
  digitalWrite(motor_2, LOW);
}

void right(){
  angle = angle + steps;
  verify();
  servo.write(angle);
    delay(100);
}

void left(){
  angle = angle - steps;
  verify();
  servo.write(angle);
  delay(100);
}

void center(){
  ang(90);
  servo.write(angle);
  delay(100);
}

void verify() {
  if(angle >= 180){
    angle = 178;
  }else if(angle <= 30){
    angle = 30;
  }
}

void measure(){
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = duration * 0.034 / 2;
}

void setup() {
        Serial.begin(38400);     // opens serial port, sets data rate to 9600 bps
        servo.attach(11);
        center();
        pinMode(trigPin, OUTPUT);
        pinMode(echoPin, INPUT);
}

void navigate(){
  while(state){
    if (Serial.available() > 0) {
      popo = Serial.read();
      if(popo=='e'){
        state = false;
      }
    }
      measure();
   center();
   if(distance < 15){
      back();
      delay(500);
      halt();
      right();
     forward();
     delay(500);
     halt();
   }else{
     forward();
    }
   if(!state){
    break;
   }
  }
}

void loop() {
        
        // send data only when you receive data:
        if (Serial.available() > 0) {
                // read the incoming byte:
                popo = Serial.read();

                // say what you got:
                Serial.print("I received: ");
                Serial.print(popo);
                Serial.print(" Angle: ");
                Serial.println(angle);
                switch (popo) {
                  case 'f':
                    forward();
                    delay(300);
                    halt();
                    break;
                  case 'b':
                    back();
                    delay(300);
                    halt();
                    break;
                  case 'r':
                    right();
                    break;
                  case 'l':
                    left();
                    break;
                  case 'n':
                    state = true;
                    navigate();
                    break;
                  case 'e':
                    state = false;
                }
        }
}
