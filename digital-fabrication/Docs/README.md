# Materials needed

## Materials

- Arduino UNO

- Ultrasonic distance snsor

- 9g Servo

- Jumpers

- L293 IC

- HC-05 Bluetooth module

- Various screws

## Manufacturing techniques

- Laser cutting to fabricate gears

- 3D printing to make chasis
