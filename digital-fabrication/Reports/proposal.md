# Indeed, it's a car

## Ideas

We want to make a small car controlled by Bluetooth using an app. I t should look something like this:

![](../images/screen_1.jpg)

We will add an ultrasonic sensor to avoid colissions. We may also make it rotating so that we can make simple maps and avoid obstacles more effectively.

![](../images/screen_4.jpg)

We will control it using MIT App Inventor, which is a very simple platform for creating simple android Apps.

![](../images/screen_3.jpg)

## Actual work

This is the first protopype for the cover shell made by Elmeri:

![](../images/screen_2.jpg)
