package oy.tol.tra;

public class Person implements Comparable<Person> {
    private String firstName;
    private String lastName;

    public Person(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getFullName() {
        return lastName + " " + firstName;
    }

    @Override
    public int compareTo(Person person) {
        String fullname = firstName + lastName;
        return fullname.compareTo(person.firstName + person.lastName);
    }

    @Override
    public boolean equals(Object person) {
        if (person instanceof Person) {
            String fullname = firstName + lastName;
            return fullname.equals(((Person) person).firstName + ((Person) person).lastName);
        }
        return false;
    }

    @Override
    public int hashCode() {
        String fullname = firstName + lastName;
        int hash = 11;
        for (int i = 0; i < fullname.length(); i++) {
            hash = hash * 29 + fullname.charAt(i);
        }
        return hash;
    }

}
