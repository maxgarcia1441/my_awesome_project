package oy.tol.tra;

public class KeyValueHashTable<K extends Comparable<K>, V> implements Dictionary<K, V> {

    private Pair<K, V>[] pairs = null;
    private int size = 0;
    private int collision = 0;
    private int reallocateCount = 0;
    private final double FILL_FACTOR = 0.6;

    public KeyValueHashTable(int capacity) throws OutOfMemoryError {
        ensureCapacity(capacity);
    }

    public KeyValueHashTable() throws OutOfMemoryError {
        ensureCapacity(20);
    }

    @Override
    public Type getType() {
        return Type.HASHTABLE;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
        if (size < 20) {
            size = 20;
        }
        pairs = (Pair<K, V>[]) new Pair[size];
    }

    @Override
    public int size() {
        return size;
    }

    /**
     * Prints out the statistics of the hash table.
     * Here you should print out member variable information which tell something
     * about your implementation.
     * <p>
     * For example, if you implement this using a hash table, update member
     * variables of the class (int counters) in add() whenever a collision
     * happen. Then print this counter value here.
     * You will then see if you have too many collisions. It will tell you that your
     * hash function is not good.
     */

    @Override
    public String getStatus() {
        String collisions = "Collisions: " + collision + "\n";
        double f = (double) (100 * size) / pairs.length;
        String fillFactor = "Fill factor : " + f + " %\n";
        String reallocations = "Number of reallocations: " + reallocateCount + "\n";
        return collisions + fillFactor + reallocations;
    }

    public int hashFunction(K key, int index, Pair<K, V>[] array) {
        int h1 = key.hashCode() % array.length;
        int h2 = 4999871 + key.hashCode() % (array.length - 1);
        int hashIndex = (((h1 << 8) + index * h2) ^ (h1 + index * (h2 << 8)) + (index * index)) & 0x7fffffff;
        return hashIndex % array.length;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (key == null || value == null) {
            throw new IllegalArgumentException("The key and value to add must not be null.");
        }
        try {
            double fillFactor = (double) size / pairs.length;
            if (fillFactor < FILL_FACTOR) {
                if (basicAdd(key, value, pairs)) {
                    return true;
                }
            } else {
                if (reallocate(pairs.length * 2)) {
                    return add(key, value);
                }
            }
            return false;
        } catch (Exception e) {
            throw new OutOfMemoryError("The memory is full");
        }
    }

    private boolean basicAdd(K key, V value, Pair<K, V>[] array) throws IllegalArgumentException, OutOfMemoryError {
        if (key == null || value == null) {
            throw new IllegalArgumentException("The key and value to add must not be null.");
        }
        try {
            Pair<K, V> pair = new Pair<K, V>(key, value);
            int i;
            for (i = 0; i < (array.length - 1); i++) {
                int hashIndex = hashFunction(key, i, array);
                if (array[hashIndex] == null) {
                    array[hashIndex] = pair;
                    size++;
                    return true;
                }
                collision++;
            }
            if (i >= (array.length - 1)) {
                for (int index = 0; index < array.length; index++) {
                    if (array[index] == null) {
                        array[index] = pair;
                        size++;
                        return true;
                    }
                }
            }
            return false;
        } catch (Exception e) {
            throw new OutOfMemoryError("The memory is full");
        }
    }

    @java.lang.SuppressWarnings({ "squid:S3012", "unchecked" })
    private boolean reallocate(int capacity) throws OutOfMemoryError {
        try {
            reallocateCount++;
            if ((pairs.length / capacity) > FILL_FACTOR) {
                capacity = (int) (pairs.length / FILL_FACTOR);
            }
            int size = 0;
            Pair<K, V>[] pairs = (Pair<K, V>[]) new Pair[capacity];
            for (Pair<K, V> pair : this.pairs) {
                if (pair != null) {
                    if (basicAdd(pair.getKey(), pair.getValue(), pairs)) {
                        size++;
                    } else {
                        return false;
                    }
                }
            }
            this.pairs = pairs;
            this.size = size;
            return true;
        } catch (Exception e) {
            throw new OutOfMemoryError("The memory is full");
        }
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (key == null) {
            throw new IllegalArgumentException("Key cannot be null.");
        }
        int i = 0;
        int hashIndex = hashFunction(key, i, pairs);
        for (i = 0; i < (pairs.length - 1); i++) {
            if (pairs[hashIndex] == null) {
                return null;
            }
            if ((pairs[hashIndex].getKey()).equals(key)) {
                return pairs[hashIndex].getValue();
            }
            hashIndex = hashFunction(key, i + 1, pairs);

        }

        if (i >= (pairs.length - 1)) {
            for (Pair<K, V> pair : pairs) {
                if (pair != null) {
                    if ((pair.getKey()).equals(key)) {
                        return pair.getValue();
                    }
                }
            }
        }
        return null;
    }

    @Override
    @java.lang.SuppressWarnings({ "unchecked" })
    public Pair<K, V>[] toSortedArray() {
        Pair<K, V>[] sortedArray = (Pair<K, V>[]) new Pair[size];
        int index = 0;
        for (Pair<K, V> pair : pairs) {
            if (pair != null) {
                sortedArray[index] = pair;
                index++;
            }
        }
        Algorithms.fastSort(sortedArray);
        return sortedArray;
    }

    @Override
    @java.lang.SuppressWarnings({ "squid:S3012", "unchecked" })
    public void compress() throws OutOfMemoryError {
        try {
            Pair<K, V>[] pairs = (Pair<K, V>[]) new Pair[size];
            int index = 0;
            for (Pair<K, V> pair : this.pairs) {
                if (pair != null) {
                    pairs[index] = pair;
                    index++;
                }
            }
            this.pairs = pairs;
        } catch (Exception e) {
            throw new OutOfMemoryError("The memory is full");
        }
    }

}