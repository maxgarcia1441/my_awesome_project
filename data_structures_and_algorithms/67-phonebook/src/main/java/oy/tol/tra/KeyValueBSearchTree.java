package oy.tol.tra;


public class KeyValueBSearchTree<K extends Comparable<K>,V> implements Dictionary<K, V> {

    // This is the BST implementation, KeyValueHashTable has the hash table implementation
    private int size = 0;
    private int collision = 0;
    private Pair<K,V>[] toSortArray = null;
    private int sortedIndex = 0;
    private Node root = null;

    private class Node {
        Node left;
        Node right;
        Pair<K,V> pair;
        Node(K key, V value) {
            pair = new Pair<K,V>(key, value);
            left = right = null;
        }
    }

    @Override
    public Type getType() {
       return Type.BST;
    }
 
    @Override
    public int size() {
        return size;
    }

    /**
     * Prints out the statistics of the tree structure usage.
     * Here you should print out member variable information which tell something about
     * your implementation.
     * <p>
     * For example, if you implement this using a hash table, update member variables of the class
     * (int counters) in add(K) whenever a collision happen. Then print this counter value here. 
     * You will then see if you have too many collisions. It will tell you that your hash function
     * is good or bad (too much collisions against data size).
     */
    @Override
    public String getStatus() {
        String depth = "Tree depth: " + maxDepth(root) + "\n";
        String collisions = "Number of collisions: " + collision + "\n";
        String nElements = "Number of elements in tree: " + size + "\n";
        return depth + collisions + nElements;
    }

    @Override
    public boolean add(K key, V value) throws IllegalArgumentException, OutOfMemoryError {
        if (key == null || value == null) {
            throw new IllegalArgumentException("The key and value to add must not be null");
        }
        try {
            Node node = new Node(key, value);
            Node x = root;
            Node parent = null;
            while (x != null) {
                parent = x;
                if (key.compareTo(x.pair.getKey()) < 0) {
                    x = x.left;
                } else if (key.compareTo(x.pair.getKey()) > 0) {
                    x = x.right;
                } else {
                    if (x.pair.getValue() != value) {
                        node.left = x.left;
                        node.right = x.right;
                        x = node;
                    }
                    collision++;
                    return true;
                }
            }
            if (root == null) {
                root = node;
                size++;
            } else if (key.compareTo(parent.pair.getKey()) < 0) {
                parent.left = node;
                size++;
            } else {
                parent.right = node;
                size++;
            }
            return true;
        } catch (Exception e) {
            throw new OutOfMemoryError("The memory is full");
        }
    }

    @Override
    public V find(K key) throws IllegalArgumentException {
        if (key == null) {
            throw new IllegalArgumentException("The key to find must not be null.");
        }
        Node node = root;
        while (node != null) {
            if((node.pair.getKey()).equals(key)){
                break;
            } else if (key.compareTo(node.pair.getKey()) < 0) {
                node = node.left;
            } else {
                node = node.right;
            }
        }
        if (node == null) {
            return null;
        }
        return node.pair.getValue();
    }

    @Override
    public void ensureCapacity(int size) throws OutOfMemoryError {
    }

    public void bstInOrder(Node node) {
        if (node != null) {
            bstInOrder(node.left);
            toSortArray[sortedIndex++] = node.pair;
            bstInOrder(node.right);
        }
    }

    @Override
    @java.lang.SuppressWarnings({"unchecked"})
    public Pair<K,V> [] toSortedArray() {
        toSortArray = (Pair<K,V>[]) new Pair[size];
        bstInOrder(root);
        return toSortArray;
    }
    
    @Override
    public void compress() throws OutOfMemoryError {
        // how ??
    }
   
    public int maxDepth(Node node) {
        if (node == null) {
            return 0;
        }
        int leftDepth = maxDepth(node.left);
        int rightDepth = maxDepth(node.right);
        if (leftDepth > rightDepth) {
            return (leftDepth + 1);
        } else {
            return (rightDepth + 1);
        }
    }
}
