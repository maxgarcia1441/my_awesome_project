package oy.tol.tra;

import java.util.function.Predicate;

public class Algorithms {
    public static <T extends Comparable<T>> void sort(T[] array) {
        int i = array.length - 2;
        while (i >= 0) {
            T temp = array[i];
            for (int e = i + 1; e <= array.length - 1; e++) {
                if (array[e - 1].compareTo(array[e]) > 0) {
                    array[e - 1] = array[e];
                    array[e] = temp;
                }
            }
            i--;
        }
    }

    // ...
    public static <T> void reverse(T[] array) {
        int i = 0;
        while (i < array.length / 2) {
            T temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
            i++;
        }
    }

    public static <T> void swap(T[] array, int e1, int e2) {
        T temp;
        temp = array[e1];
        array[e1] = array[e2];
        array[e2] = temp;
    }

    public static class ModeSearchResult<T> {
        public T theMode;
        public int count = 0;
    }

    public static <T extends Comparable<T>> ModeSearchResult<T> findMode(T[] array) {
        ModeSearchResult<T> result = new ModeSearchResult<>();
        if (array != null && array.length > 1) {
            sort(array);
            result.count = 1;
            int currentC = 1;
            result.theMode = array[0];
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i].compareTo(array[i + 1]) == 0) {
                    currentC++;
                } else {
                    currentC = 1;
                }
                if (currentC >= result.count) {
                    result.count = currentC;
                    result.theMode = array[i];
                }
            }
        } else {
            result.theMode = null;
            result.count = -1;
        }
        return result;
    }

    public static <T> int partitionByRule(T[] array, int count, Predicate<T> rule) {
        int index = 0;
        int nextIndex = 0;
        boolean found = false;
        for (int i = 0; i < count; i++) {
            if (rule.test(array[i])) {
                index = i;
                found = true;
                break;
            }
        }
        if (found == false) {
            return count;
        }
        nextIndex = index + 1;
        while (nextIndex < count) {
            if (!rule.test(array[nextIndex])) {
                swap(array, index, nextIndex);
                index = index + 1;
            }
            nextIndex++;
        }
        return index;
    }

    public static <T extends Comparable<T>> int binarySearch(T aValue, T[] fromArray, int fromIndex, int toIndex) {
        if (fromIndex > toIndex) {
            return -1;
        } else {
            int pivot = (fromIndex + toIndex) / 2;
            if (fromArray[pivot].equals(aValue)) {
                return pivot;
            } else if (fromArray[pivot].compareTo(aValue) > 0) {
                return binarySearch(aValue, fromArray, fromIndex, pivot - 1);
            } else {
                return binarySearch(aValue, fromArray, pivot + 1, toIndex);
            }
        }
    }

    public static <T extends Comparable<T>> int partition(T[] array, int low, int high){
        T pivot = array[high];
        int i = low - 1;
        for(int j = low; j < high; j++){
            if(array[j].compareTo(pivot) < 0){
                i++;
                swap(array, i, j);
            }
        }
        swap(array, i + 1, high);
        return i + 1;
    }

    public static <T extends Comparable<T>> void quickSort(T[] array, int low, int high){
        if(low < high){
            int pIndex = partition(array, low, high);
            quickSort(array, low, pIndex - 1);
            quickSort(array, pIndex + 1, high);
        }
    }

    public static <T extends Comparable<T>> void fastSort(T[] array){
        quickSort(array, 0, array.length - 1);
    }

}
