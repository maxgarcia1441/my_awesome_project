## Report 67-PHONEBOOK

# Hash Functions

I first tried using the `hashCode()` methodto generate the indexes, but that turned out to be quite useless, as it only uses a single parameter and generates the same result over and over.
Then I tried multiplying the index by prime numbers and the `i` variable, but that produced values very close to each other, and the array didn't fill up properly.
Then as I was suggested, binary operations were the answer, first the bit shifting `<<` which allows to move all the bits usually concentrated to the right of the number.
Then the bitwise operators `&` *(AND)* and `^` *(XOR) allow to manipulate the whole number in a way that produce more dispersed numbers, that in a way seem more random from a human perspective.

# Fill factors / Growing the hash table

I tried using the suggested fill factor in the lectures at first, but for *unkknown* reasons the number *0.6* was faster. I tried *0.1* just for curiosity, but it produced a crazy amount of reallocations, which are not particularly fast.

![Performance test results](time.jfif)

## BST

The tree seemed to get somewhat deeper with some hash functions, I believe it is due to values concentrating on one side of the tree, not properly dispersing.

## Collisions

Using the number of collisions as an argument for the hash function, whenever there was one, a new hashcode was generated. When taht failed avoiding collisions, `add()` simply looked for the first free place in the array starting from index 0 in ascending order.

## Different solutions

### Hash Table
At the very beginning of the task I couldn't stop getting stack overflow errors, I solved it by changing the way the table is reallocated, the hash function was also quite challenging, I kept getting too many collisions until I used the bit wise operations.
*(At some opint I used random numbers in* `add()` * but it was very diffuclt to find the values back).*

### BST

Solution was straightforward following lecutre and online material.

## Sorting Algorithm

I used quick sort

## Feedback

The course was quite well implemented, lectures are quite clear. Exercies are quite challenging but they definitely ensure you understand the theory behind the course.