# Report

## Strings

![](strings.png)

The algorithm itself should have a time complexity of O(n) as it goes through elements in a sorted array.
The graph nevertheless seems to have a complexity of O(n^2), this is because the algorithm first has to sort the array using insertion sort (in my case), and only then start looking for the mode.

## Doubles

![](doubles.png)

The algorithm used is the same, and so the time complexity should be similar, but it seems to be quite faster (like if it was multiplied by some constant). I believe this is due to how string and doubles are compared, JVM uses a *String constant pool*, which is a separate block of memory, and there are extra steps needed to access the String objects than Doubles, and so extra delays are added for each comparison made.

## Comments

Both graphs seem to have spikes at some points, probably due to how my *(very slow)* computer allocates resources.
