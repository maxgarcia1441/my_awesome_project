package oy.tol.tra;

public class LinkedListImplementation<E> implements LinkedListInterface<E> {

   private class Node<T> {
      Node(T data) {
         element = data;
         next = null;
      }

      T element;
      Node<T> next;

      @Override
      public String toString() {
         return element.toString();
      }
   }

   private Node<E> head = null;
   private int size = 0;

   @Override
   public void add(E element) throws NullPointerException, LinkedListAllocationException {
      if (element == null) {
         throw new NullPointerException();
      } else {
         try {
            Node<E> newNode = new Node<E>(element);
            if (head == null) {
               head = newNode;
            } else {
               Node<E> tail = head;
               while (tail.next != null) {
                  tail = tail.next;
               }
               tail.next = newNode;
            }
            size++;
         } catch (Exception e) {
            throw new LinkedListAllocationException(null);
         }
      }
   }

   @Override
   public void add(int index, E element)
         throws NullPointerException, LinkedListAllocationException, IndexOutOfBoundsException {
      if ((index >= size && index != 0) || index < 0) {
         throw new IndexOutOfBoundsException();
      } else if (element == null) {
         throw new NullPointerException();
      } else {
         try {
            Node<E> newNode = new Node<E>(element);
            if (index == 0) {
               newNode.next = head;
               head = newNode;
               size++;
            } else {
               Node<E> indexNode = head;
               for (int i = 0; i < index - 1; i++) {
                  indexNode = indexNode.next;
               }
               Node<E> temp = indexNode.next;
               indexNode.next = newNode;
               newNode.next = temp;
               size++;
            }
         } catch (Exception e) {
            throw new LinkedListAllocationException(null);
         }
      }
   }

   @Override
   public boolean remove(E element) throws NullPointerException {
      boolean found = false;
      if (element == null) {
         throw new NullPointerException();
      } else {
         Node<E> tail = head;
         for (int i = 0; i < size - 1; i++) {
            if (tail.next.element.equals(element)) {
               tail.next = tail.next.next;
               size--;
               found = true;
               break;
            }
            if (tail.next == null) {
               break;
            }
            tail = tail.next;
         }
      }
      return found;
   }

   @Override
   public E remove(int index) throws IndexOutOfBoundsException {
      if (index >= size || index < 0) {
         throw new IndexOutOfBoundsException();
      } else {
         E data;
         if (index == 0) {
            data = head.element;
            head = head.next;
         } else {
            Node<E> tail = head;
            for (int i = 0; i < index - 1; i++) {
               tail = tail.next;
            }
            data = tail.next.element;
            if (index != size - 1) {
               tail.next = tail.next.next;
            } else {
               tail.next = null;
            }
         }
         size--;
         return data;
      }
   }

   @Override
   public E get(int index) throws IndexOutOfBoundsException {
      if (index < 0 || index >= size) {
         throw new IndexOutOfBoundsException();
      } else {
         Node<E> tail = head;
         if (index == 0) {
            return head.element;
         }
         for (int i = 0; i < index - 1; i++) {
            tail = tail.next;
         }
         return tail.next.element;
      }
   }

   @Override
   public int indexOf(E element) throws NullPointerException {
      if (element == null) {
         throw new NullPointerException();
      } else if (size == 0) {
         return -1;
      } else {
         int index = 0;
         boolean found = false;
         Node<E> tail = head;
         if (head.element.equals(element)) {
            return 0;
         }
         if (size > 1) {
            while (tail.next != null) {
               if (tail.next.element.equals(element)) {
                  index++;
                  found = true;
                  break;
               }
               tail = tail.next;
               index++;
            }
         }
         if (found) {
            return index;
         } else {
            return -1;
         }
      }
   }

   @Override
   public int size() {
      return size;
   }

   @Override
   public void clear() {
      head = null;
      size = 0;
   }

   @Override
   public void reverse() {
      if (size != 0) {
         Node<E> first = head;
         Node<E> second = first.next;
         Node<E> third;
         if (size >= 3) {
            while (second.next != null) {
               third = second.next;
               second.next = first;
               first = second;
               second = third;
               third = second.next; // if second.next != null do this;
            }
            second.next = first;
            head = second;
         } else if (size == 2) {
            first.next = null;
            second.next = head;
            head = second;
         }
      }
   }

   @Override
   public String toString() {
      String text = "[";
      if (size > 0) {
         Node<E> tail = head;
         while (tail.next != null) {
            text = text + String.valueOf(tail) + ", ";
            tail = tail.next;
         }
         text = text + String.valueOf(tail);
      }
      text = text + "]";
      return text;
   }

}
