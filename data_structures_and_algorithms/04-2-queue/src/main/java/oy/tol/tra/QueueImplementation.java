package oy.tol.tra;

public class QueueImplementation<E> implements QueueInterface<E> {
    private static final int DEFAULT_CAPACITY = 10;
    private int capacity;
    private Object[] mainArray;
    private int size;
    private int head;
    private int tail;

    public QueueImplementation() {
        this(DEFAULT_CAPACITY);
    }

    public QueueImplementation(int capacity) {
        this.capacity = capacity;
        this.mainArray = new Object[capacity];
        head = 0;
        tail = -1;
        size = 0;
    }

    @Override
    public int capacity() {
        return capacity;
    }

    @Override
    public void enqueue(E element) throws QueueAllocationException, NullPointerException{
        if (element == null) {
            throw new NullPointerException();
        } else {
            if (size == capacity) {
                try {
                    // CREATE A NEW RRAY AND TRANSFER ALL
                    Object[] newArray = new Object[capacity * 2];
                    for (int i = 0; i < capacity; i++) {
                        newArray[i] = mainArray[head];
                        head++;
                        if (head == capacity) {
                            head = 0;
                        }
                    }
                    head = 0;
                    tail = capacity - 1;
                    mainArray = null;
                    mainArray = newArray;
                    capacity = capacity * 2;
                } catch (Exception e) {
                    throw new QueueAllocationException(null);
                }
            }
            tail++;
            size++;
            if (tail == capacity) {
                tail = 0;
            }
            mainArray[tail] = (Object) element;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public E dequeue() throws QueueIsEmptyException {
        if (size == 0) {
            throw new QueueIsEmptyException(null);
        }
        Object temp = mainArray[head];
        mainArray[head] = null;
        head++;
        if (head == capacity) {
            head = 0;
        }
        size--;
        return (E) temp;
    }

    @SuppressWarnings("unchecked")
    public E element() throws QueueIsEmptyException {
        if (size == 0) {
            throw new QueueIsEmptyException(null);
        }
        return (E) mainArray[head];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void clear() {
        mainArray = null;
        mainArray = new Object[capacity];
        size = 0;
        head = 0;
        tail = -1;
    }

    public String toString() {
        String text = "[";
        if (size > 0) {
            int index = head;
            int next;
            if(tail == capacity - 1){
                next = 0;
            }else{
                next = tail + 1;
            }
            for (int i = 0; i < capacity; i++) {
                if (mainArray[index] != null) {
                    text = text + String.valueOf(mainArray[index]);
                }
                index++;
                if (index == capacity) {
                    index = 0;
                }
                if (index == next) {
                    break;
                } else {
                        text = text + ", ";
                }
            }
        }
        text = text + "]";
        return text;
    }
}
