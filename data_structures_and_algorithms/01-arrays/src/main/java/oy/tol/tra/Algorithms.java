package oy.tol.tra;

public class Algorithms {
    public static <T extends Comparable<T>> void sort(T[] array) {
        int i = array.length - 2;
        while (i >= 0) {
            T temp = array[i];
            for (int e = i + 1; e <= array.length - 1; e++) {
                if (array[e - 1].compareTo(array[e]) > 0) {
                    array[e - 1] = array[e];
                    array[e] = temp;
                }
            }
            i--;
        }
    }

    // ...
    public static <T> void reverse(T[] array) {
        int i = 0;
        while (i < array.length / 2) {
            T temp = array[i];
            array[i] = array[array.length - i - 1];
            array[array.length - i - 1] = temp;
            i++;
        }
    }

    public static <T> void swap(T[] array, int e1, int e2) {
        T temp;
        temp = array[e1];
        array[e1] = array[e2];
        array[e2] = temp;
    }

}
