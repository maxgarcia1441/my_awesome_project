# TIRA/DSA-2022

This repository contains the exercises and course projects of the Data Structures and Algorithms (TIRA, Tietorakenteet ja algoritmit in Finnish) course.

Completed by **Max Garcia Hinojosa**

## About

* Course material for Tietorakenteet ja algoritmit | Data structures and algorithms 2021-2022.
* Study Program for Information Processing Science, Department of Information Technology and Electrical Engineering, University of Oulu.
* Antti Juustila, INTERACT Research Group (English implementation also Pertti Karhapää, M3S Research Group).