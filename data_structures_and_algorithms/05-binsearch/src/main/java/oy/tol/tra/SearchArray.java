package oy.tol.tra;

/**
 * For searching a number from an array of integers.
 * 
 * @author Antti Juustila
 * @version 1.0
 */
public class SearchArray {

   private SearchArray() {
      // Empty
   }

   public static <T extends Comparable<T>> int slowLinearSearch(T aValue, T[] fromArray, int fromIndex, int toIndex){
      for (int index = fromIndex; index < toIndex; index++) {
         if (fromArray[index].equals(aValue)) {
            return index;
         }
      }
      return -1;
   }

}
