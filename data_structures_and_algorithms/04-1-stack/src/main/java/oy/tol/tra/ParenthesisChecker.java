package oy.tol.tra;

/**
 * Uses the StackInterface implementation to check that parentheses in text
 * files
 * match.
 * <p>
 * StackImplementation
 * to check if parentheses in the two test files match or not.
 * <p>
 * NOTE: The Person.json test file has an error, but the tests expect that. So
 * the test will
 * not fail with that file -- the erroneus json file is _expected_.
 * <p>
 * Note that you do not have to instantiate this class anywhere. The
 * ParenthesisTests:
 * <ul>
 * <li>Constructs your implementation of the
 * {@code StackImplementation<Character>}, and
 * <li>Calls ParenthesisChecker.checkParentheses.
 * </ul>
 * So your job is just to have a working StackImplementation and implement the
 * ParenthesisChecker.checkParentheses.
 * Then execute the ParenthesisTests.
 */
public class ParenthesisChecker {

   private ParenthesisChecker() {
   }

   /**
    * opening and closing
    * parentheses. It should check for matching parentheses:
    * <p>
    * <code>Lorem ipsum ( dolor sit {  amet, [ consectetur adipiscing ] elit, sed } do eiusmod tempor ) incididunt ut...</code>,
    * <p>
    * which can be found for example in Java source code and JSON structures.
    * <p>
    * If the string has issues with parentheses, you should throw a
    * {@code ParenthesisException} with a
    * descriptive message and error code. Error codes are already defined for you
    * in the ParenthesesException
    * class to be used.
    * <p>
    * What is to be tested:
    * <ul>
    * <li>when an opening parenthesis is found in the string, it is successfully
    * pushed to the stack.
    * <li>when a closing parenthesis is found in the string, a matching opening
    * parenthesis is popped from the stack.
    * <li>when popping a parenthesis from the stack, it must not be null. Otherwise
    * string has too many closing parentheses.
    * <li>when the string has been handled, and if the stack still has parentheses,
    * there are too few closing parentheses.
    * </ul>
    * 
    * @param stack      The stack object used in checking the parentheses from the
    *                   string.
    * @param fromString A string containing parentheses to check if it is valid.
    * @return Returns the number of parentheses found from the input in total (both
    *         opening and closing).
    * @throws ParenthesesException     if the parentheses did not match as
    *                                  intended.
    * @throws StackAllocationException If the stack cannot be allocated or
    *                                  reallocated if necessary.
    */
   public static int checkParentheses(StackInterface<Character> stack, String fromString) throws ParenthesesException {
      int n = 0;

      char[] charArray = fromString.toCharArray();
      for (char c : charArray) {
         if (Character.compare(c, '{') == 0 || Character.compare(c, '(') == 0 || Character.compare(c, '[') == 0) {
            n++;
            try {
               stack.push(c);
            } catch (Exception e) {
               throw new ParenthesesException(fromString, ParenthesesException.STACK_FAILURE);
            }
         }
         switch (c) {
            case '}':
               n++;
               if (stack.size() == 0) {
                  throw new ParenthesesException(fromString, ParenthesesException.TOO_MANY_CLOSING_PARENTHESES);
               } else if (Character.compare(stack.pop(), '{') != 0)
                  throw new ParenthesesException(fromString, ParenthesesException.PARENTHESES_IN_WRONG_ORDER);
               break;
            case ')':
               n++;
               if (stack.size() == 0) {
                  throw new ParenthesesException(fromString, ParenthesesException.TOO_MANY_CLOSING_PARENTHESES);
               } else if (Character.compare(stack.pop(), '(') != 0)
                  throw new ParenthesesException(fromString, ParenthesesException.PARENTHESES_IN_WRONG_ORDER);
               break;
            case ']':
               n++;
               if (stack.size() == 0) {
                  throw new ParenthesesException(fromString, ParenthesesException.TOO_MANY_CLOSING_PARENTHESES);
               } else if (Character.compare(stack.pop(), '[') != 0)
                  throw new ParenthesesException(fromString, ParenthesesException.PARENTHESES_IN_WRONG_ORDER);
               break;
         }
      }
      if (!stack.isEmpty())
         throw new ParenthesesException(fromString, ParenthesesException.TOO_FEW_CLOSING_PARENTHESES);
      return n;
   }
}
