package oy.tol.tra;

/**
 * An implementation of the StackInterface.
 * <p>
 * 
 * Note that you need to implement construtor(s) for your concrete
 * StackImplementation, which
 * allocates the internal Object array for the Stack:
 * - a default constructor, calling the StackImplementation(int size) with value
 * of 10.
 * - StackImplementation(int size), which allocates an array of Object's with
 * size.
 * -- remember to maintain the capacity and/or currentIndex when the stack is
 * manipulated.
 */
public class StackImplementation<E> implements StackInterface<E> {

   private static final int MY_CONSTANT_VARIABLE = 10;
   private int capacity;
   private Object[] itemArray;
   private int top;

   /**
    * Allocates a stack with a default capacity.
    * 
    * @throws StackAllocationException
    */
   public StackImplementation() throws StackAllocationException {
      this(MY_CONSTANT_VARIABLE);
   }

   /**
    * 
    * 
    * @param capacity The capacity of the stack.
    * @throws StackAllocationException If cannot allocate room for the internal
    *                                  array.
    */
   public StackImplementation(int capacity) throws StackAllocationException {
      if (capacity < 2) {
         throw new StackAllocationException("");
      }
      this.capacity = capacity;
      try {
         itemArray = new Object[capacity];
      } catch (Exception e) {
         throw new StackAllocationException("cannot allocate room for internal array");
      }
      top = -1;
   }

   @Override
   public int capacity() {
      return capacity;
   }

   @Override
   public void push(E element) throws StackAllocationException, NullPointerException {
      if(element == null) throw new NullPointerException();
      if (top + 1 >= itemArray.length) {
         try {
            Object[] copy = itemArray;
            itemArray = null;
            itemArray = new Object[capacity * 2];
            for (int i = 0; i < capacity; i++) {
               itemArray[i] = copy[i];
            }
            capacity = itemArray.length;
         } catch (Exception e) {
            throw new StackAllocationException(null);
         }
      }
      itemArray[top + 1] = element;
      top++;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E pop() throws StackIsEmptyException {
      if (isEmpty())
         throw new StackIsEmptyException(null);
      Object temp = itemArray[top];
      itemArray[top] = null;
      top--;
      return (E) temp;
   }

   @SuppressWarnings("unchecked")
   @Override
   public E peek() throws StackIsEmptyException {
      try {
         Object topObject = itemArray[top];
      } catch (Exception e) {
         throw new StackIsEmptyException("No elements in the stack");
      }
      return (E) itemArray[top];
   }

   @Override
   public int size() {
      return top + 1;
   }

   @Override
   public void clear() {
      itemArray = null;
      itemArray = new Object[capacity];
      top = -1;
   }

   @Override
   public boolean isEmpty() {
      boolean isEmpty = false;
      if (top == -1)
         isEmpty = true;
      return isEmpty;
   }

   @Override
   public String toString() {
      String arrayString = "[";
      if (isEmpty()) {
      } else if (size() == 1) {
         if(itemArray[0] != null) arrayString = arrayString + String.valueOf(itemArray[0]);
      } else {
         if(itemArray[0] != null) arrayString = arrayString + String.valueOf(itemArray[0]);
         for (int i = 1; i < itemArray.length; i++) {
            if(itemArray[i] != null) arrayString = arrayString + ", " + String.valueOf(itemArray[i]);
         }
      }
      arrayString = arrayString + "]";
      return arrayString;
   }
}
