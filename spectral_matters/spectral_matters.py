"""
spectral_matters

Project for "Elementary programming (Autumn 2021)"

@author Max Garcia Hinojosa
"""


import guilib as gb
import os
import numpy as np
state = {
    "points": [(1, 1)],
    "plot_counter": 0,
    "files_opened": 0,
    "is_plot": False,
    "is_correct_line": False,
    "edit_mode": False,
    "slope_mode": False,
    "area_mode": False,
    "first_used": False,
    "n_files_opened": 0
}


widgets = {
    "button_1": ""
}

e_values = {
    "placeholder": []
}

i_values = {
    "placeholder": []
}

area_values = {
    "placeholder": []
}


def read_files():
    """
    It catches common errors when opening files
    """
    try:
        filtered_files()
    except UnicodeError:
        gb.open_msg_window("Error", "The files are damaged", error=True)
    except ValueError:
        gb.open_msg_window("Error", "The files are damaged", error=True)
    except FileNotFoundError:
        gb.open_msg_window("Error", "Select a valid folder", error=True)
    except PermissionError:
        gb.open_msg_window("Error", "Select a valid folder", error=True)


def filtered_files():
    """
    Reads all data files in the directory specified by the user, adds the intensities
    and puts them back in a dictionary with a predtermined name
    """
    state["n_files_opened"] += 1
    state["name"] = "Plot_{}".format(state["n_files_opened"])
    state["directory_path"] = gb.open_folder_dialog("Select folder")
    content = os.listdir(state["directory_path"])
    file_path = state["directory_path"] + "/" + content[1]
    content.remove(content[0])
    e_values[state["name"]] = []
    i_values[state["name"]] = []
    with open(file_path) as file:
        for count, row in enumerate(file.readlines()):
            row = row.split(" ")
            e_values[state["name"]].append(float(row[0].strip()))
            i_values[state["name"]].append(float(row[1].strip()))
    for filename in content:
        file_path = state["directory_path"] + "/" + filename
        with open(file_path) as file:
            for count, row in enumerate(file.readlines()):
                row = row.split(" ")
                i_values[state["name"]][count] += float(row[1].strip())
    state["files_opened"] += 1


def select_plot():
    """
    It plots the selected (previously loaded) plot
    """
    try:
        i, state["name"] = gb.read_selected(widgets["listbox_selector"])
        create_plot()
    except KeyError:
        gb.open_msg_window("Error", "No plot has been selected")


def update_points():
    """
    It verifies only two points selected by the user are going to be used
    """
    if len(state["points"]) > 2:
        state["points"].clear()
        state["edit_mode"] = False


def choose_point(event):
    """
    Receives a mouse event from a mouse click and reads the x and y data
    values from it. It calls other functions depending on the state
    of the program
    """
    state["x"], state["y"] = event.xdata, event.ydata
    x, y = state["x"], state["y"]
    gb.update_label(widgets["label_pos"], "Currently working in: {}".format(state["name"]))
    state["points"].append((x, y))
    if len(state["points"]) == 2 and state["edit_mode"]:
        p1 = state["points"][0]
        p2 = state["points"][1]
        update_labels()
        x1, y1 = float(p1[0]), float(p1[1])
        x2, y2 = float(p2[0]), float(p2[1])
        state["slope"] = (y2 - y1) / (x2 - x1)
        if state["area_mode"]:
            calculate_area()
            state["area_mode"] = False
            state["edit_mode"] = False
        elif state["slope_mode"]:
            guiding_plot()
            state["slope_mode"] = False
            state["edit_mode"] = False
    update_points()


def update_labels():
    """
    Prints the coordinates of the clicks in a label
    """
    try:
        p_1 = state["points"][0]
        p_2 = state["points"][1]
        gb.update_label(widgets["pos_label_1"], "X1 at: {:.2f}, Y1 at: {:.2f}".format(p_1[0], p_1[1]))
    except TypeError:
        pass
    else:
        p_1 = state["points"][0]
        p_2 = state["points"][1]
        gb.update_label(widgets["pos_label_1"], "X1 at: {:.2f}, Y1 at: {:.2f}".format(p_1[0], p_1[1]))
        gb.update_label(widgets["pos_label_2"], "X1 at: {:.2f}, Y1 at: {:.2f}".format(p_2[0], p_2[1]))

# This error really doesn't affect anything, but it's annoying


def act_calculate_area():
    """
    It enables the area mode, so that the intensity can be calculated
    """
    gb.open_msg_window("", "Select two points of the graph")
    state["edit_mode"] = True
    state["area_mode"] = True
    state["points"].clear()


def calculate_area():
    """
    Uses the coordinates from the clicks and finds the closest energy
    and intensity values so that the are below the line can be calculated
    :return:
    """
    points = state["points"]
    x1, y1 = points[0][0], points[0][1]
    x2, y2 = points[1][0], points[1][1]

# Extract values within the parameter set by the clicks from e_values and i_values
    distances_1, distances_2 = [], []
    for e_value in e_values[state["name"]]:
        distances_1.append(abs(e_value - x1))
        distances_2.append(abs(e_value - x2))
    a_1 = distances_1.index(min(distances_1))
    a_2 = distances_2.index(min(distances_2))
    indexes = [a_1, a_2]
    p_1, p_2 = min(indexes), max(indexes)
    try:
        area_values[state["name"]]
    except KeyError:
        area_values[state["name"]] = []
    area_values[state["name"]].append((np.trapz(i_values[state["name"]][p_1:p_2], e_values[state["name"]][p_1:p_2]),
                                      (p_1, p_2)))
# Prints the most recently calculated area
    counter = 1
    gb.update_label(widgets["area_label"], "Area in interval {} at plot {} is: {:.2f}".format(
        str(counter),
        str(state["name"]),
        area_values[state["name"]][-1][0]))


def create_plot_interface():
    """
    Creates user interface once a plot has been loaded
    """
    if not state["first_used"]:
        widgets["frame_plot_menu_up"] = gb.create_frame(widgets["frame_plot_menu"], gb.TOP)
        widgets["frame_plot_menu_down"] = gb.create_frame(widgets["frame_plot_menu"], gb.TOP)
        widgets["plot_menu_1"] = gb.create_button(widgets["frame_plot_menu_up"], "Edit plot", correct_slope)
        widgets["textfield_name"] = gb.create_textfield(widgets["frame_plot_menu_down"])
        widgets["plot_menu_2"] = gb.create_button(widgets["frame_plot_menu_down"], "Plot and save", create_correct_plot)
        widgets["button_area"] = gb.create_button(widgets["frame_plot_menu_down"], "Calculate area", act_calculate_area)
        widgets["save"] = gb.create_button(widgets["frame_plot_menu_down"], "Save as picture", save_image)
        widgets["save_all"] = gb.create_button(widgets["frame_1"], "Save all", save_all)
        widgets["button_open"] = gb.create_button(widgets["frame_2_2_RIGHT"], "Open Plot", select_plot)
        state["first_used"] = True
        widgets["canvas"], widgets["figure"], widgets["subplot"] = gb.create_figure(widgets["frame_plot_plot"],
                                                                                    choose_point, 700, 350)


def create_plot():
    """
    Plots energy and intensity values
    """
    try:
        test = e_values[state["name"]]
    except KeyError:
        gb.open_msg_window("Error", "There are no values to plot", error=True)
    else:
        create_plot_interface()
        # Erase previous plots
        widgets["subplot"].clear()
        # Plots selected value sets
        widgets["subplot"].plot(e_values[state["name"]], i_values[state["name"]])
        widgets["subplot"].set_xlabel("Binding energy: (eV)")
        widgets["subplot"].set_ylabel("Intensity: Arbitrary Units")
        widgets["subplot"].set_title(state["name"])
        widgets["canvas"].draw()
        state["is_plot"] = True


def display_contents():
    """
    It shows the names of the previously loaded plots
    """
    plot_names = list(e_values.keys())
    plot_names.remove("corrective_line")
    plot_names.remove("placeholder")
    print(plot_names)
    gb.add_list_row(widgets["listbox_selector"], plot_names[0])
    for i in plot_names:
        gb.remove_list_row(widgets["listbox_selector"], 0)
    for i in plot_names:
        gb.add_list_row(widgets["listbox_selector"], i)


def create_correct_plot():
    """
    It corrects the slope of the graph using the corrective
    line drawn by the user
    """
    new_name = gb.read_field(widgets["textfield_name"])
    if "slope" not in list(state.keys()):
        gb.open_msg_window("Error", "No changes have been made yet", error=True)
    elif new_name and new_name not in list(e_values.keys()):
        gb.clear_field(widgets["textfield_name"])
        widgets["subplot"].clear()
        temp_e = e_values[state["name"]]
        temp_i = i_values[state["name"]]
# Assign name from textfield
        state["name"] = new_name
# Y = slope * x + B
        e_values[state["name"]] = temp_e
        i_values[state["name"]] = []
        inter = temp_i[1] - state["slope"] * temp_e[1]
        for i, i_value in enumerate(temp_i):
            i_values[state["name"]].append(i_value - (state["slope"] * temp_e[i]) - inter)
        create_plot()
        display_contents()
    elif not new_name:
        gb.open_msg_window("Error", "To save the plot enter a name on the textfield first", error=True)
    elif new_name in list(e_values.keys()):
        gb.open_msg_window("Error", "A file with that name already exists", error=True)
        gb.clear_field(widgets["textfield_name"])


def save_image():
    """
    Saves the plot as an image to a location specified by
    the user
    """
    path = gb.open_folder_dialog("Select folder")
    widgets["figure"].savefig(path + "/" + state["name"] + ".png")


def save_all():
    """
    Saves the energy and intensity value for every plot that was loaded,
    it also saves the areas below the curves in separate files
    """
    path = gb.open_folder_dialog("Select folder to save files")
    try:
        os.mkdir(path + "/spectraaaal")
        path = path + "/spectraaaal"
    except FileExistsError:
        gb.open_msg_window("Error", "Folder already exists")
    else:
        to_save = list(e_values.keys())
        to_save.remove("corrective_line")
        to_save.remove("placeholder")
        for name in to_save:
            with open(path + "/" + name + ".txt", "a+") as f:
                for i, value in enumerate(e_values[name]):
                    f.write("{} {}\n".format(value, i_values[name][i]))
        intensities = list(area_values.keys())
        intensities.remove("placeholder")
        for name in intensities:
            with open(path + "/" + name + "_intensities" + ".txt", "a+") as f:
                for value in area_values[name]:
                    f.write("{}\n".format(value))


def add_line_guide():
    """
    It calculates the values required to plot a corrective line
    """
    p1 = state["points"][0]
    xp1, yp1 = float(p1[0]), float(p1[1])
    x1, x2 = e_values[state["name"]][1], e_values[state["name"]][-1]
    y1 = (state["slope"] * x1) + (yp1 - (state["slope"] * xp1))
    y2 = (state["slope"] * x2) + (yp1 - (state["slope"] * xp1))
    e_values["corrective_line"] = [x1, x2]
    i_values["corrective_line"] = [y1, y2]
    widgets["subplot"].plot(e_values["corrective_line"], i_values["corrective_line"])


def guiding_plot():
    """
    It creates a line over the original plot based on the clicks
    from the user to serve as a guide
    """
    widgets["subplot"].clear()
    add_line_guide()
    widgets["subplot"].plot(e_values[state["name"]], i_values[state["name"]])
    widgets["canvas"].draw()


def correct_slope():
    """
    It enables the "slope" mode so that the user can choose two
    points in the screen to correct the slope of the graph
    """
    gb.open_msg_window("", "Select two points of the graph")
    state["edit_mode"] = True
    state["slope_mode"] = True
    state["points"].clear()


def main():
    """
        Creates the user interface
    """
    widgets["main_window"] = gb.create_window("Spectral matters")
    widgets["frame_1"] = gb.create_frame(widgets["main_window"], gb.LEFT)
    widgets["frame_2"] = gb.create_frame(widgets["main_window"], gb.LEFT)
    widgets["frame_plot"] = gb.create_frame(widgets["frame_2"], gb.TOP)
    widgets["frame_plot_plot"] = gb.create_frame(widgets["frame_plot"], gb.LEFT)
    widgets["frame_plot_menu"] = gb.create_frame(widgets["frame_plot"], gb.LEFT)
    widgets["frame_2_2"] = gb.create_frame(widgets["frame_2"], gb.RIGHT)
    widgets["frame_2_labels"] = gb.create_frame(widgets["frame_2"], gb.RIGHT)
    widgets["frame_2_2_LEFT"] = gb.create_frame(widgets["frame_2_2"], gb.LEFT)
    widgets["frame_2_2_RIGHT"] = gb.create_frame(widgets["frame_2_2"], gb.LEFT)
    widgets["label_pos"] = gb.create_label(widgets["frame_2_2_LEFT"], "")
    widgets["plots_label"] = gb.create_label(widgets["frame_2_2_LEFT"], "Saved plots")
    widgets["listbox_selector"] = gb.create_listbox(widgets["frame_2_2_LEFT"], width=55, height=7)
    widgets["pos_label_1"] = gb.create_label(widgets["frame_2_labels"], "")
    widgets["pos_label_2"] = gb.create_label(widgets["frame_2_labels"], "")
    widgets["area_label"] = gb.create_label(widgets["frame_2_labels"], "")
    widgets["slt_file_bt"] = gb.create_button(widgets["frame_1"], "Select files", read_files)
    widgets["quit"] = gb.create_button(widgets["frame_1"], "Quit", quit)
    widgets["create_plot"] = gb.create_button(widgets["frame_1"], "Plot values", create_plot)
    widgets["frame_2_3"] = gb.create_frame(widgets["frame_2"], gb.TOP)
    gb.start()


if __name__ == "__main__":
    main()
