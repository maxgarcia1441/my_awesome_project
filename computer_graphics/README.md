# Computer Graphics

This is are exercises for the University of Oulu course **521140S-3004 Computer Graphics**.
The course then progresses to three programming assignments that cover different aspects of computer graphics.

## a1

a1 focuses on setting up the development environment and ensuring that the required libraries are properly installed.

## First task

The first assignment focuses on geometry and involves tasks such as rotating an object and viewing it from different angles. Students learn about manipulating geometric objects and understanding their spatial transformations.

## Second task

The second assignment delves into texture mapping, where it is taught how to define a cube and assign UV coordinates to its surfaces. It explores the process of mapping textures onto objects, showing how textures can enhance the visual appearance of rendered scenes.

## Third task

The final assignment is about hierarchical models. It demonstrates how to define complex models composed of interconnected parts and helps understand how to animate the motion of different parts within the model. Rendering techniques are also explored, finally rendering the finished model in a visibly appealing manner.
