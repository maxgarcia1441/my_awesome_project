# Task 2

- Completed by Max Garcia Hinojosa

The program defines a 2x2x2 cube centered at the origin and applies texture mapping to its surfaces.

The code defines the vertices of the cube using coordinate values. Each side of the cube is formed by a combination of vertices, and the triangles that make up each side are defined in counter-clockwise orientation. The cube has a total of 12 triangles, representing its six sides. The vertices are assigned names from v1 to v8.

To map textures onto the cube, the code assigns texture coordinates to each vertex. Texture coordinates determine how the texture is applied to the surface of an object. The texture coordinates are specified for each triangle in the cube, corresponding to the vertices defined earlier.

## Requirements

> Follow the installation steps in the root directory

## Usage

To run the project, navigate to the root directory and run the following command:

    python CG_assignment1.py

This will open the OpenGL window, where you can see the body displayed. You can then use the keyboard and mouse to move the camera around.

Use the arrow keys to move the camera left, right, up, and down.
Use the mouse to rotate the view.

![](images/cube_1.png)

> This is the cube in it's simplest form, right after all the triangles were defined.

![](images/cube_2.png)

> The first texture was mapped to the cube.

![](images/cube_3.png)

> Manually mapping a second texture.

![](images/cube_4.png)

> Inverting the colors by subtracting the RGB channels from 1.

`color = (1 - texture(texture_sampler, uv)).rgb;`

![](images/cube_5.png)

> Redefining `vertex_shader.glsl` to scale the body in the Y-axis.