# Task 1

- Max Garcia Hinojosa

This task demonstrates how to display a body using Python OpenGL and apply a rotation matrix to it. It also shows how to move the camera around using input from the keyboard and mouse.

## Requirements

> Follow the installation steps in the root directory

## Usage

To run the project, navigate to the root directory and run the following command:

    python CG_assignment1.py

This will open the OpenGL window, where you can see the body displayed. You can then use the keyboard and mouse to move the camera around.

Use the arrow keys to move the camera left, right, up, and down.
Use the mouse to rotate the view.

![](images/screen_1.png)

> This is the body used as reference, before applying any transformations

![](images/screen_4.png)

> Once traslation and rotation were implemented, the camera can now move around the scene

