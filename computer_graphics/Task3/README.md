# Task 2
- Completed by Max Garcia Hinojosa

## Solar System Simulation

The program simulates the Sun, Earth, and Moon using a simplified model. Here are the main tasks performed by the program:

- Define the vertices and indices for an icosahedron, which serves as the base shape for the objects in the scene.
- Generate a sphere by subdividing each face of the icosahedron multiple times.
- Calculate UV coordinates for each vertex of the sphere to map textures onto the objects.
- Set up the OpenGL context, including compiling shaders, loading textures, and creating vertex buffers.
- Tasks
    - Implement the draw method in the Win class to render the objects in the scene.
    - Draw the Moon: The Moon is rendered as a separate object with its own rotation and translation transformations.
    ![](images/screen_3.png)
    - Rotate the Sun, Earth, and Moon: Each object is rotated based on the current time to create an animation effect.
    - Revolution of the Earth and Moon: The Earth and Moon objects are translated around the Sun to simulate their orbital motion.
    ![](images/screen_4.png)

## Usage

To run the project, navigate to the root directory and run the following command:

```
python CG_assignment1.py
```

Make sure you have the necessary dependencies installed (OpenGL, PyOpenGL, glm)

![](images/screen_1.png)

> *Voilà!*